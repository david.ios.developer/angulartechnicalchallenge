import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '@app/core/models/user.model';
import { LocalStorageService } from '@app/core/services/LocalStorageService';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  user?: UserModel;
  constructor(private router: Router) {
    this.user = LocalStorageService.getItem('user');
  }

  logout(): void {
    LocalStorageService.clear();
    this.router.navigateByUrl('login');
  }
}
