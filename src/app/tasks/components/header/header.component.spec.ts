import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { TASKS_MODULE_TEST } from '@app/tasks/task.module.test';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...TASKS_MODULE_TEST,
      providers: [...(TASKS_MODULE_TEST.providers as [])],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*constructor(private router: Router) {
    this.user = LocalStorageService.getItem('user');
  }*/

  /*logout(): void {
    LocalStorageService.clear();
    this.router.navigateByUrl('login');
  }*/
});
