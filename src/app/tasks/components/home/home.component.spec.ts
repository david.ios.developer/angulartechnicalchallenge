import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TASKS_MODULE_TEST } from '@app/tasks/task.module.test';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...TASKS_MODULE_TEST,
      providers: [...(TASKS_MODULE_TEST.providers as [])],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
