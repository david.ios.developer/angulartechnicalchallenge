import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddEditTaskComponent } from '../add-edit-task/add-edit-task.component';
import { TasksService } from '@app/core/services/tasksServices/tasks.service';
import { MatSort } from '@angular/material/sort';
import { CommonService } from '@app/common/utils/common.services';
import { Messages } from '@app/common/utils/constants';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { AlertWidth } from '@app/common/utils/magic.numbers';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrl: './task-list.component.css',
})
export class TaskListComponent implements OnInit {
  columns: string[] = ['title', 'description', 'completed', 'edit', 'delete'];
  tasksList = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private dialog: MatDialog,
    private taskService: TasksService,
    private common: CommonService
  ) {}

  ngOnInit(): void {
    this.getTaskList();
  }

  getTaskList(): void {
    this.taskService.getTasksList().subscribe({
      next: (res) => {
        this.tasksList = new MatTableDataSource(res);
        this.tasksList.sort = this.sort;
        this.tasksList.paginator = this.paginator;
      },
      error: console.log,
    });
  }

  addTask(): void {
    const dialogRef = this.dialog.open(AddEditTaskComponent);
    dialogRef.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.getTaskList();
        }
      },
    });
  }

  editTask(data: any): void {
    const dialogRef = this.dialog.open(AddEditTaskComponent, { data });
    dialogRef.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.getTaskList();
        }
      },
    });
  }

  filterList(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tasksList.filter = filterValue.trim().toLowerCase();

    if (this.tasksList.paginator) {
      this.tasksList.paginator.firstPage();
    }
  }

  deleteTask(id: string): void {
    this.taskService.deleteTask(id).subscribe({
      next: (res) => {
        this.common.snackBar(Messages.DELETED_SUCCESFULLY, Messages.ACCEPT);
        this.getTaskList();
      },
      error: console.log,
    });
  }

  deleteTaskPrompt(id: string): void {
    const dialogRef = this.alertDialog(Messages.DELETE_PROMPT);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteTask(id);
      }
    });
  }

  completeTaskPrompt(data: any): void {
    if (!data.completed) {
      const dialogRef = this.alertDialog(Messages.COMPLETE_PROMPT);
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          var newData = data;
          newData.completed = true;
          this.completeTask(newData);
        }
      });
    }
  }

  completeTask(data: any): void {
    this.taskService.updateTask(data.id, data).subscribe({
      next: (val: any) => {
        this.common.snackBar(Messages.UPDATED_SUCCEFULLY);
        this.getTaskList();
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  alertDialog(message: string): MatDialogRef<AlertDialogComponent, any> {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: AlertWidth,
      data: { message: message },
    });
    return dialogRef;
  }
}
