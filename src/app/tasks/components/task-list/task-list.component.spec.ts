import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TaskListComponent } from './task-list.component';
import { TASKS_MODULE_TEST } from '@app/tasks/task.module.test';

describe('TaskListComponent', () => {
  let component: TaskListComponent;
  let fixture: ComponentFixture<TaskListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...TASKS_MODULE_TEST,
      providers: [...(TASKS_MODULE_TEST.providers as [])],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(TaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
