import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AlertDialogComponent } from './alert-dialog.component';
import { TASKS_MODULE_TEST } from '@app/tasks/task.module.test';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

describe('AlertDialogComponent', () => {
  let component: AlertDialogComponent;
  let fixture: ComponentFixture<AlertDialogComponent>;
  const dialog = jasmine.createSpyObj('MatDialogRef', ['close']);
  const common = jasmine.createSpyObj('CommonService', ['snackBar']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...TASKS_MODULE_TEST,
      providers: [
        ...(TASKS_MODULE_TEST.providers as []),
        {
          provide: MatDialogRef,
          useValue: dialog,
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(AlertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
