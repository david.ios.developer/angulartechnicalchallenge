import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaskModel } from '@app/core/models/task.model';
import {
  LongTextValidations,
  Messages,
  ShortTextValidations,
} from '@app/common/utils/constants';
import { TasksService } from '@app/core/services/tasksServices/tasks.service';
import { CommonService } from '@app/common/utils/common.services';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserModel } from '@app/core/models/user.model';
import { LocalStorageService } from '@app/core/services/LocalStorageService';

@Component({
  selector: 'app-add-edit-task',
  templateUrl: './add-edit-task.component.html',
  styleUrl: './add-edit-task.component.css',
})
export class AddEditTaskComponent {
  user: UserModel;
  task?: TaskModel;
  formGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private taskService: TasksService,
    private dialogRef: MatDialogRef<AddEditTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private common: CommonService
  ) {
    this.formGroup = this.fb.group({
      title: ['', ShortTextValidations],
      description: ['', LongTextValidations],
    });
    this.user = LocalStorageService.getItem('user');
  }

  ngOnInit(): void {
    this.loadForm();
    console.log(this.data);
  }

  loadForm(): void {
    if (this.data) {
      this.formGroup.patchValue(this.data);
    }
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      if (this.data) {
        this.updateTask();
      } else {
        this.addTask();
      }
    }
  }

  updateTask(): void {
    this.taskService
      .updateTask(this.data.id, this.createTaskObject())
      .subscribe({
        next: (val: any) => {
          this.common.snackBar(Messages.UPDATED_SUCCEFULLY);
          this.dialogRef.close(true);
        },
        error: (err: any) => {
          console.error(err);
        },
      });
  }

  addTask(): void {
    this.taskService.addTask(this.createTaskObject()).subscribe({
      next: (val: any) => {
        this.common.snackBar(Messages.ADDED_SUCCEFULLY);
        this.dialogRef.close(true);
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  createTaskObject(): TaskModel {
    let task: TaskModel = {
      title: this.formGroup.get('title')?.value ?? '',
      description: this.formGroup.get('description')?.value,
      completed: this.data?.completed ?? false,
      userId: this.user.id,
    };
    return task;
  }
}
