import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AddEditTaskComponent } from './add-edit-task.component';
import { TASKS_MODULE_TEST } from '@app/tasks/task.module.test';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TasksService } from '@app/core/services/tasksServices/tasks.service';
import { Observable } from 'rxjs';

fdescribe('AddEditTaskComponent', () => {
  let component: AddEditTaskComponent;
  let fixture: ComponentFixture<AddEditTaskComponent>;
  let taskService: TasksService;

  const common = jasmine.createSpyObj('CommonService', ['snackBar']);
  const dialog = jasmine.createSpyObj('MatDialogRef', ['close']);

  let mockData = {
    id: '1',
    title: 'This is a title',
    description: 'this is a description',
    completed: false,
    userId: '1',
  };

  let newTask = {
    title: 'New title',
    description: 'New description',
    completed: false,
    userId: '1',
  };

  let user = {
    id: '1',
    name: 'Juan',
    lastname: 'Pérez',
    email: 'juan@email.com',
    password: '123456',
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...TASKS_MODULE_TEST,
      providers: [
        ...(TASKS_MODULE_TEST.providers as []),
        {
          provide: MatDialogRef,
          useValue: dialog,
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(AddEditTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    taskService = TestBed.inject(TasksService);
    component.user = user;
    component.data = mockData;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load form', () => {
    component.loadForm();
    const formulario = component.formGroup;
    formulario.controls['title'].setValue('This is a title');
    formulario.controls['description'].setValue('This is a long description');
    expect(formulario.valid).toBeTrue();
  });

  it('should load form and validate', () => {
    component.loadForm();
    const formulario = component.formGroup;
    formulario.controls['title'].setValue('');
    formulario.controls['description'].setValue('');
    expect(formulario.valid).toBeFalse();
  });

  it('should add task', () => {
    const addTask = spyOn(component, 'addTask').and.callFake(() => ({}));
    component.loadForm();
    component.data = undefined;
    const formulario = component.formGroup;
    formulario.controls['title'].setValue('This is a title');
    formulario.controls['description'].setValue('This is a long description');
    fixture.detectChanges();
    component.onSubmit();
    expect(addTask).toHaveBeenCalled();
  });

  it('should update task', () => {
    const updateTask = spyOn(component, 'updateTask').and.callFake(() => ({}));
    component.data = mockData;
    component.loadForm();
    const formulario = component.formGroup;
    formulario.controls['title'].setValue('This is a title');
    formulario.controls['description'].setValue('This is a long description');
    fixture.detectChanges();
    component.onSubmit();
    expect(updateTask).toHaveBeenCalled();
  });

  it('should execute udpate task', () => {
    spyOn(taskService, 'updateTask').and.returnValue(
      new Observable((obs) => {
        obs.next(mockData);
        expect(common.snackBar).toHaveBeenCalled();
        expect(dialog.close).toHaveBeenCalled();
      })
    );
  });

  it('should execute add task', () => {
    spyOn(taskService, 'addTask').and.returnValue(
      new Observable((obs) => {
        obs.next(mockData);
        expect(common.snackBar).toHaveBeenCalled();
        expect(dialog.close).toHaveBeenCalled();
      })
    );
  });

  it('should create', () => {
    component.loadForm();
    component.data = newTask;
    const formulario = component.formGroup;
    formulario.controls['title'].setValue(newTask.title);
    formulario.controls['description'].setValue(newTask.description);
    fixture.detectChanges();
    let data = component.createTaskObject();
    expect(data).toEqual(newTask);
  });
});
