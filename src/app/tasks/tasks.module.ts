import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '@app/core/services/authServices/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { AddEditTaskComponent } from './components/add-edit-task/add-edit-task.component';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from '@app/common/utils/material.module';


/* Material Modules */
/*import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';*/

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    TaskListComponent,
    AddEditTaskComponent,
    AlertDialogComponent,
  ],
  exports: [],
  providers: [AuthService],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
  ],
})
export class HomeModule {}
