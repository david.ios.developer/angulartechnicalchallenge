import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '@app/common/utils/common.services';
import { Messages, ShortTextValidations } from '@app/common/utils/constants';
import { AuthService } from '@app/core/services/authServices/auth.service';
import { LocalStorageService } from '@app/core/services/LocalStorageService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  formGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private common: CommonService
  ) {
    this.formGroup = this.fb.group({
      email: [
        '',
        {
          validators: [Validators.required, Validators.email],
          updateOn: 'change',
        },
      ],
      password: ['', ShortTextValidations],
    });
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.authService.login(this.formGroup.value).subscribe({
        next: (val: any) => {
          if (val.length) {
            LocalStorageService.setItem('user', val[0]);
            this.router.navigateByUrl('home');
          } else {
            this.common.snackBar(Messages.USER_NOT_FOUND);
          }
        },
        error: (err: any) => {
          console.error(err);
        },
      });
    }
  }
}
