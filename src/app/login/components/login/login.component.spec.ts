import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { LOGIN_TEST_MODULE } from '@app/login/login.test.module';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      ...LOGIN_TEST_MODULE,
      providers: [...(LOGIN_TEST_MODULE.providers as [])],
    }).compileComponents();
  }));

  beforeEach(async () => {
    TestBed.configureTestingModule({}).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
