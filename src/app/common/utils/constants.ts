import { Validators } from '@angular/forms';

export const ShortTextValidations = {
  validators: [
    Validators.maxLength(25),
    Validators.required,
    Validators.pattern(/[A-Za-z0-9]+/g),
  ],
  updateOn: 'submit',
};

export const LongTextValidations = {
  validators: [
    Validators.maxLength(100),
    Validators.required,
    Validators.pattern(/[A-Za-z0-9]+/g),
  ],
  updateOn: 'submit',
};

export const Messages = {
  DELETED_SUCCESFULLY: 'Tarea borrada exitosamente!',
  UPDATED_SUCCEFULLY: 'Tarea editada exitosamente!',
  ADDED_SUCCEFULLY: 'Tarea agregada exitosamente!',
  DELETE_PROMPT: 'Realmente desea borrar esta tarea?',
  COMPLETE_PROMPT: 'Desea marcar esta tarea como completada?',
  USER_NOT_FOUND: 'Usuario no encontrado',
  ACCEPT: 'Aceptar',
  CANCEL: 'Cancelar',
};
