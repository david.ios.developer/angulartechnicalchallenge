export interface TaskModel {
  id?: string;
  title: string;
  description: string;
  completed?: boolean;
  userId: string;
}

export interface TaskActionModel {
  task?: TaskModel;
  action?: Action;
}

export enum Action {
  created,
  updated,
  deleted,
  canceled,
}
