export interface LoginModel {
  email: string;
  password: string;
}

export interface UserModel {
  id: string;
  name?: string;
  lastName?: string;
  email: string;
  password: string;
}
