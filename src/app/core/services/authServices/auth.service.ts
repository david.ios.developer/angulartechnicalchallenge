import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginModel } from '@app/core/models/user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private baseUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  login(user: LoginModel): Observable<any> {
    let params = new HttpParams()
      .set('email', user.email)
      .set('password', user.password);
    return this.http.get(`${this.baseUrl}/users`, { params });
  }
}
