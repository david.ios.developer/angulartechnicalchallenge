import { TestBed } from '@angular/core/testing';
import { TasksService } from './tasks.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('TasksService', () => {
  let service: TasksService;
  let httpController: HttpTestingController;
  let baseUrl = 'http://localhost:3000';

  let mockTasks = [
    {
      title: 'algo',
      description: 'otra cosa',
      completed: false,
      userId: '2',
      id: '1',
    },
  ];
  let mockUser = [
    {
      id: '1',
      name: 'Juan',
      lastname: 'Pérez',
      email: 'juan@email.com',
      password: '123456',
    },
  ];
  let newTask = {
    title: 'New title',
    description: 'New description',
    completed: false,
    userId: '1',
    id: '1',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(TasksService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get tasks list', () => {
    service.getTasksList().subscribe((data) => {
      expect(data).toEqual(mockTasks);
    });
    const url = `${baseUrl}/tasks`;
    const req = httpController.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(mockTasks);
  });

  it('should add new task', () => {
    service.addTask(newTask).subscribe((data) => {
      expect(data).toEqual(mockTasks);
    });
    const url = `${baseUrl}/tasks`;
    const req = httpController.expectOne(url);
    expect(req.request.method).toEqual('POST');
    req.flush(mockTasks);
  });

  it('should update task', () => {
    service.updateTask('1', newTask).subscribe((data) => {
      expect(data).toEqual(mockTasks);
    });
    const url = `${baseUrl}/tasks/1`;
    const req = httpController.expectOne(url);
    expect(req.request.method).toEqual('PUT');
    req.flush(mockTasks);
  });

  it('should delete tasks', () => {
    service.deleteTask('1').subscribe((data) => {
      expect(data).toEqual(mockTasks);
    });
    const url = `${baseUrl}/tasks/1`;
    const req = httpController.expectOne(url);
    expect(req.request.method).toEqual('DELETE');
    req.flush(mockTasks);
  });
});
